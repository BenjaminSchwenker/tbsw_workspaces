
# ---General settings---

[general]
# Determines the fit range of angle dists: 
# Fit from first bin above max/(fitrange_par*e) to last above max/(fitrange_par*e)
fitrange_parameter:1.0

# Beam momemtum at position (u,v) on target plane is calculated:
# 
# momentum = BEmean +  BEugradient*u + BEvgradient*v

# Choose a lamdba start value
lambda:1.0

# Choose a momentumoffset start value [GeV]
momentumoffset:3.0

# Choose a BEugradient start value [GeV/mm]
momentumugradient:0.0

# Choose a BEvgradient start value [GeV/mm]
momentumvgradient:0.0

# Choose fit model, either highland or moliere
model:highland

# The energy loss due to bremstrahlung is modelled by a weighted mean of the average energy before and
# after the transition through the material:
#
# p=par[0]*((1-epsilon)+epsilon*exp(-XX0))
#
# In order to not include the energy loss just set epsilon to 0.0
epsilon:0.0

# Vertex multiplicity cut parameters
vertex_multiplicity_min : 1
vertex_multiplicity_max : 1


# ---X0 image settings---

[x0image]

# Name of the results file
resultsfilename : X0-completeimage

# Max fit chi2ndof value
maxchi2ndof : 5.0							

# u and v length of complete X0 image  in mm
u_length : 20.0
v_length : 20.0

# umin and vmax of the complete X0 image in mm
umin : -10.0
vmax : +10.0

# Pixel sizes of the image in µm
u_pixel_size : 400.0
v_pixel_size : 400.0

# Minimum number of tracks
# Should be in the order of 400 to 500, so that at least 800 to 1000
# scattering angles are present in the angle projected distributions 
# to ensure a valid fit with reasonable statistical uncertainties
min_tracks : 400

# Angle histogram range
histo_range : 5.0

# Number of bins in angle histogram
num_bins : 100

# Fit options
fit_options : RMELS


# ---X0 calibration settings---

[x0calibration]

# Correct decentralized angle distributions: 0(no), 1(yes)
correctmean:1

# Fix lambda parameter: 0(no), 1(yes)
fixlambda:0

# Fix momentum offset parameter: 0(no), 1(yes)
fix_momentumoffset:0

# Fix momentum u gradient parameter: 0(no), 1(yes)
fix_momentumugradient:0

# Fix momentum u gradient parameter: 0(no), 1(yes)
fix_momentumvgradient:0

# Use log likelihood estimator: 
# 0 Use chi2
# 1 Use likelihood
use_loglikelihood:1

# Angle histogram range
cali_histo_range:5.0

# Number of bins in angle histogram
cali_num_bins:100

# ---particle settings---

# Beam particle mass in GeV 
particle.mass:0.000511

# Beam particle charge in e
particle.charge:1


# ---Grid settings---

# Center of central measurement area in mm
grid.offsetu:1.6
grid.offsetv:-0.5

# Parameters, which define the orientation of the grid on the target plane
grid.mirror_u:1
grid.mirror_v:0
grid.switch_uv:true


# ---Measurement area settings---
# Measurement areas are single rectangular areas on the target u-v plane with known material properties 

# Use area in fit: 0(no), 1(yes)
MA1.exist:1

# Center position in mm
MA1.ucenter:0.0

# Center position in mm
MA1.vcenter:0.0

# Side length in mm
MA1.ulength:1.0

# Side length in mm
MA1.vlength:1.0

# Thickness in mm
MA1.thickness:0.0

# Atomic number Z
MA1.atomicnumber:13.0

# Atomic mass A
MA1.atomicmass:27.0

# Density in g/cm³
MA1.density:2.7

# Smallest run number to be used (-1 to disable)
MA1.minrunnumber:3394

# Largest run number to be used (-1 to disable)
MA1.maxrunnumber:3395

# Limit number of angles in distribution to thsi value (-1 use all available angles)
MA1.maxanglenumber:25000

MA2.exist:1            	
MA2.ucenter:4.0
MA2.vcenter:2.0
MA2.ulength:1.0
MA2.vlength:1.0
MA2.thickness:0.0
MA2.atomicnumber:13.0
MA2.atomicmass:27.0
MA2.density:2.7
MA2.minrunnumber:3394		    
MA2.maxrunnumber:3395	
MA2.maxanglenumber:25000	    

MA3.exist:1            	
MA3.ucenter:4.0
MA3.vcenter:-2.0
MA3.ulength:1.0
MA3.vlength:1.0
MA3.thickness:0.0
MA3.atomicnumber:13.0
MA3.atomicmass:27.0
MA3.density:2.7
MA3.minrunnumber:3394		    
MA3.maxrunnumber:3395	
MA3.maxanglenumber:25000

MA4.exist:1            	
MA4.ucenter:-4.0
MA4.vcenter:2.0
MA4.ulength:1.0
MA4.vlength:1.0
MA4.thickness:0.0
MA4.atomicnumber:13.0
MA4.atomicmass:27.0
MA4.density:2.7
MA4.minrunnumber:3394		    
MA4.maxrunnumber:3395	
MA4.maxanglenumber:25000

MA5.exist:1            	
MA5.ucenter:-4.0
MA5.vcenter:-2.0
MA5.ulength:1.0
MA5.vlength:1.0
MA5.thickness:0.0
MA5.atomicnumber:13.0
MA5.atomicmass:27.0
MA5.density:2.7
MA5.minrunnumber:3394		    
MA5.maxrunnumber:3395	
MA5.maxanglenumber:25000

MA6.exist:1
MA6.ucenter:0.0
MA6.vcenter:0.0
MA6.ulength:1.0
MA6.vlength:1.0
MA6.thickness:0.5
MA6.atomicnumber:13.0
MA6.atomicmass:27.0
MA6.density:2.7
MA6.minrunnumber:3387
MA6.maxrunnumber:3388
MA6.maxanglenumber:25000

MA7.exist:1            	
MA7.ucenter:4.0
MA7.vcenter:2.0
MA7.ulength:1.0
MA7.vlength:1.0
MA7.thickness:0.5
MA7.atomicnumber:13.0
MA7.atomicmass:27.0
MA7.density:2.7
MA7.minrunnumber:3387		    
MA7.maxrunnumber:3388	
MA7.maxanglenumber:25000	    

MA8.exist:1            	
MA8.ucenter:4.0
MA8.vcenter:-2.0
MA8.ulength:1.0
MA8.vlength:1.0
MA8.thickness:0.5
MA8.atomicnumber:13.0
MA8.atomicmass:27.0
MA8.density:2.7
MA8.minrunnumber:3387		    
MA8.maxrunnumber:3388	
MA8.maxanglenumber:25000

MA9.exist:1            	
MA9.ucenter:-4.0
MA9.vcenter:2.0
MA9.ulength:1.0
MA9.vlength:1.0
MA9.thickness:0.5
MA9.atomicnumber:13.0
MA9.atomicmass:27.0
MA9.density:2.7
MA9.minrunnumber:3387		    
MA9.maxrunnumber:3388	
MA9.maxanglenumber:25000

MA10.exist:1            	
MA10.ucenter:-4.0
MA10.vcenter:-2.0
MA10.ulength:1.0
MA10.vlength:1.0
MA10.thickness:0.5
MA10.atomicnumber:13.0
MA10.atomicmass:27.0
MA10.density:2.7
MA10.minrunnumber:3387		    
MA10.maxrunnumber:3388	
MA10.maxanglenumber:25000

MA11.exist:1
MA11.ucenter:0.0
MA11.vcenter:0.0
MA11.ulength:1.0
MA11.vlength:1.0
MA11.thickness:1.0
MA11.atomicnumber:13.0
MA11.atomicmass:27.0
MA11.density:2.7
MA11.minrunnumber:3374
MA11.maxrunnumber:3375
MA11.maxanglenumber:25000

MA12.exist:1            	
MA12.ucenter:4.0
MA12.vcenter:2.0
MA12.ulength:1.0
MA12.vlength:1.0
MA12.thickness:1.0
MA12.atomicnumber:13.0
MA12.atomicmass:27.0
MA12.density:2.7
MA12.minrunnumber:3374		    
MA12.maxrunnumber:3375	
MA12.maxanglenumber:25000	    

MA13.exist:1            	
MA13.ucenter:4.0
MA13.vcenter:-2.0
MA13.ulength:1.0
MA13.vlength:1.0
MA13.thickness:1.0
MA13.atomicnumber:13.0
MA13.atomicmass:27.0
MA13.density:2.7
MA13.minrunnumber:3374		    
MA13.maxrunnumber:3375	
MA13.maxanglenumber:25000

MA14.exist:1            	
MA14.ucenter:-4.0
MA14.vcenter:2.0
MA14.ulength:1.0
MA14.vlength:1.0
MA14.thickness:1.0
MA14.atomicnumber:13.0
MA14.atomicmass:27.0
MA14.density:2.7
MA14.minrunnumber:3374		    
MA14.maxrunnumber:3375	
MA14.maxanglenumber:25000

MA15.exist:1            	
MA15.ucenter:-4.0
MA15.vcenter:-2.0
MA15.ulength:1.0
MA15.vlength:1.0
MA15.thickness:1.0
MA15.atomicnumber:13.0
MA15.atomicmass:27.0
MA15.density:2.7
MA15.minrunnumber:3374		    
MA15.maxrunnumber:3375	
MA15.maxanglenumber:25000

MA16.exist:1
MA16.ucenter:0.0
MA16.vcenter:0.0
MA16.ulength:1.0
MA16.vlength:1.0
MA16.thickness:1.5
MA16.atomicnumber:13.0
MA16.atomicmass:27.0
MA16.density:2.7
MA16.minrunnumber:3367
MA16.maxrunnumber:3368
MA16.maxanglenumber:25000

MA17.exist:1            	
MA17.ucenter:4.0
MA17.vcenter:2.0
MA17.ulength:1.0
MA17.vlength:1.0
MA17.thickness:1.5
MA17.atomicnumber:13.0
MA17.atomicmass:27.0
MA17.density:2.7
MA17.minrunnumber:3367		    
MA17.maxrunnumber:3368	
MA17.maxanglenumber:25000	    

MA18.exist:1            	
MA18.ucenter:4.0
MA18.vcenter:-2.0
MA18.ulength:1.0
MA18.vlength:1.0
MA18.thickness:1.5
MA18.atomicnumber:13.0
MA18.atomicmass:27.0
MA18.density:2.7
MA18.minrunnumber:3367		    
MA18.maxrunnumber:3368	
MA18.maxanglenumber:25000

MA19.exist:1            	
MA19.ucenter:-4.0
MA19.vcenter:2.0
MA19.ulength:1.0
MA19.vlength:1.0
MA19.thickness:1.5
MA19.atomicnumber:13.0
MA19.atomicmass:27.0
MA19.density:2.7
MA19.minrunnumber:3367		    
MA19.maxrunnumber:3368	
MA19.maxanglenumber:25000

MA20.exist:1            	
MA20.ucenter:-4.0
MA20.vcenter:-2.0
MA20.ulength:1.0
MA20.vlength:1.0
MA20.thickness:1.5
MA20.atomicnumber:13.0
MA20.atomicmass:27.0
MA20.density:2.7
MA20.minrunnumber:3367		    
MA20.maxrunnumber:3368	
MA20.maxanglenumber:25000

