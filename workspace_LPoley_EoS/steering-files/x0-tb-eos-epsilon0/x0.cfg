
# ---General settings---

[general]
# Determines the fit range of angle dists: 
# Fit from first bin above max/(fitrange_par*e) to last above max/(fitrange_par*e)
fitrange_parameter:1.0

# Beam momemtum at position (u,v) on target plane is calculated:
# 
# momentum = BEmean +  BEugradient*u + BEvgradient*v

# Choose a lamdba start value
lambda:1.0

# Choose a momentumoffset start value [GeV]
momentumoffset:2.0

# Choose a BEugradient start value [GeV/mm]
momentumugradient:0.0

# Choose a BEvgradient start value [GeV/mm]
momentumvgradient:0.0

# Choose fit model, either highland or moliere
model:highland

# The energy loss due to bremstrahlung is modelled by a weighted mean of the average energy before and
# after the transition through the material:
#
# p=par[0]*((1-epsilon)+epsilon*exp(-XX0))
#
# In order to not include the energy loss just set epsilon to 0.0
epsilon:0.0

# Vertex multiplicity cut parameters
vertex_multiplicity_min : 1
vertex_multiplicity_max : 1


# ---X0 image settings---

[x0image]

# Name of the results file
resultsfilename : X0-completeimage

# Max fit chi2ndof value
maxchi2ndof : 50.0	

# X/X0 image setup:
# There are a few basic definitions needed to generate a X/X0 image on the scattering target.
# First you will have to decide where on the target plane the image should be located and how
# large the overall image area is supposed to be. In case you are interested in the whole measured
# area, it is a good idea to use an image size, that is roughly the size of the M26 acceptance 
# (20 mm x 10 mm). In a typical setup the center of the u,v coordinate origin is very close to
# the center of the beam spot. It is therefore a good idea to choose a u_min=-u_length/2 and
# v_max=v_length/2. The size of the image pixels is a tradeoff between a good spatial resolution
# and the ability for an accurate estimation of the X/X0 value. For a stable and unbiased fit
# the reconstructed angle distributions in each pixel should have at least 2500 entries. The
# complete image is divided into a number of partial images with 75 x 75 pixels. In case the total
# number of pixels in any of the two directions (for example: u_length/u_pixel_size) isn't a 
# multitude of 75 one last partial image will be added and the overall image will be slightly
# larger than originally intented.

# u and v length of complete X0 image in mm
u_length : 20.0
v_length : 20.0

# umin and vmax of the complete X0 image in mm
umin : -11.0
vmax : +9.75

# Pixel sizes of the image in µm
u_pixel_size : 400.0
v_pixel_size : 400.0

# Minimum number of tracks
# Should be in the order of 400 to 500, so that at least 800 to 1000
# scattering angles are present in the angle projected distributions 
# to ensure a valid fit with reasonable statistical uncertainties
min_tracks : 400

# Angle histogram range (in units of the standard deviation of the individual histograms)
histo_range : 5.0

# Number of bins in angle histogram
num_bins : 150

# Fit options
fit_options : RMELS


# ---X0 calibration settings---

[x0calibration]

# Correct decentralized angle distributions: 0(no), 1(yes)
correctmean:1

# Fix lambda parameter: 0(no), 1(yes)
fixlambda:0

# Fix momentum offset parameter: 0(no), 1(yes)
fix_momentumoffset:0

# Fix momentum u gradient parameter: 0(no), 1(yes)
fix_momentumugradient:0

# Fix momentum u gradient parameter: 0(no), 1(yes)
fix_momentumvgradient:0

# Use log likelihood estimator: 
# 0 Use chi2
# 1 Use likelihood
use_loglikelihood:1

# Angle histogram range
cali_histo_range:5.0

# Number of bins in angle histogram
cali_num_bins:200

# ---particle settings---

# Beam particle mass in GeV 
particle.mass:0.000511

# Beam particle charge in e
particle.charge:1


# ---Grid settings---

# Center of central measurement area in mm
grid.offsetu:1.6
grid.offsetv:-0.5

# Parameters, which define the orientation of the grid on the target plane
grid.mirror_u:1
grid.mirror_v:0
grid.switch_uv:true


# ---Measurement area settings---
# Measurement areas are single rectangular areas on the target u-v plane with known material properties 

# Use area in fit: 0(no), 1(yes)
MA1.exist:1

# Center position in mm
MA1.ucenter:0.0

# Center position in mm
MA1.vcenter:-0.5

# Side length in mm
MA1.ulength:1.5

# Side length in mm
MA1.vlength:1.5

# Thickness in mm
MA1.thickness:0.0

# Atomic number Z (not needed in case highland model was selected)
MA1.atomicnumber:13.0

# Atomic mass A (not needed in case highland model was selected)
MA1.atomicmass:27.0

# Density in g/cm³ (not needed in case highland model was selected)
MA1.density:2.7

# X0 in mm (not needed in case moliere model was selected)
MA1.X0:88.97

# Smallest run number to be used (-1 to disable)
MA1.minrunnumber:208

# Largest run number to be used (-1 to disable)
MA1.maxrunnumber:213

# Limit number of angles in distribution to thsi value (-1 use all available angles)
MA1.maxanglenumber:50000

MA2.exist:1            	
MA2.ucenter:-6.0
MA2.vcenter:2.0
MA2.ulength:1.5
MA2.vlength:1.5
MA2.thickness:0.0
MA2.atomicnumber:13.0
MA2.atomicmass:27.0
MA2.density:2.7
MA2.minrunnumber:208		    
MA2.maxrunnumber:213	
MA2.maxanglenumber:50000	    

MA3.exist:1            	
MA3.ucenter:-6.0
MA3.vcenter:-3.0
MA3.ulength:1.5
MA3.vlength:1.5
MA3.thickness:0.0
MA3.atomicnumber:13.0
MA3.atomicmass:27.0
MA3.density:2.7
MA3.minrunnumber:208		    
MA3.maxrunnumber:213	
MA3.maxanglenumber:50000

MA4.exist:1            	
MA4.ucenter:6.0
MA4.vcenter:2.0
MA4.ulength:1.5
MA4.vlength:1.5
MA4.thickness:0.0
MA4.atomicnumber:13.0
MA4.atomicmass:27.0
MA4.density:2.7
MA4.minrunnumber:208		    
MA4.maxrunnumber:213	
MA4.maxanglenumber:50000

MA5.exist:1            	
MA5.ucenter:6.0
MA5.vcenter:-3.0
MA5.ulength:1.5
MA5.vlength:1.5
MA5.thickness:0.0
MA5.atomicnumber:13.0
MA5.atomicmass:27.0
MA5.density:2.7
MA5.minrunnumber:208		    
MA5.maxrunnumber:213	
MA5.maxanglenumber:50000


MA6.exist:1
MA6.ucenter:0.0
MA6.vcenter:-0.5
MA6.ulength:1.5
MA6.vlength:1.5
MA6.thickness:4.0
MA6.atomicnumber:13.0
MA6.atomicmass:27.0
MA6.density:2.7
MA6.minrunnumber:152
MA6.maxrunnumber:157
MA6.maxanglenumber:50000

MA7.exist:1            	
MA7.ucenter:-6.0
MA7.vcenter:2.0
MA7.ulength:1.5
MA7.vlength:1.5
MA7.thickness:4.0
MA7.atomicnumber:13.0
MA7.atomicmass:27.0
MA7.density:2.7
MA7.minrunnumber:152		    
MA7.maxrunnumber:157	
MA7.maxanglenumber:50000	    

MA8.exist:1            	
MA8.ucenter:-6.0
MA8.vcenter:-3.0
MA8.ulength:1.5
MA8.vlength:1.5
MA8.thickness:4.0
MA8.atomicnumber:13.0
MA8.atomicmass:27.0
MA8.density:2.7
MA8.minrunnumber:152		    
MA8.maxrunnumber:157	
MA8.maxanglenumber:50000

MA9.exist:1            	
MA9.ucenter:6.0
MA9.vcenter:2.0
MA9.ulength:1.5
MA9.vlength:1.5
MA9.thickness:4.0
MA9.atomicnumber:13.0
MA9.atomicmass:27.0
MA9.density:2.7
MA9.minrunnumber:152		    
MA9.maxrunnumber:157	
MA9.maxanglenumber:50000

MA10.exist:1            	
MA10.ucenter:6.0
MA10.vcenter:-3.0
MA10.ulength:1.5
MA10.vlength:1.5
MA10.thickness:4.0
MA10.atomicnumber:13.0
MA10.atomicmass:27.0
MA10.density:2.7
MA10.minrunnumber:152		    
MA10.maxrunnumber:157	
MA10.maxanglenumber:50000


MA11.exist:1
MA11.ucenter:0.0
MA11.vcenter:-0.5
MA11.ulength:1.5
MA11.vlength:1.5
MA11.thickness:6.0
MA11.atomicnumber:13.0
MA11.atomicmass:27.0
MA11.density:2.7
MA11.minrunnumber:159
MA11.maxrunnumber:164
MA11.maxanglenumber:50000

MA12.exist:1            	
MA12.ucenter:-6.0
MA12.vcenter:2.0
MA12.ulength:1.5
MA12.vlength:1.5
MA12.thickness:6.0
MA12.atomicnumber:13.0
MA12.atomicmass:27.0
MA12.density:2.7
MA12.minrunnumber:159		    
MA12.maxrunnumber:164	
MA12.maxanglenumber:50000	    

MA13.exist:1            	
MA13.ucenter:-6.0
MA13.vcenter:-3.0
MA13.ulength:1.5
MA13.vlength:1.5
MA13.thickness:6.0
MA13.atomicnumber:13.0
MA13.atomicmass:27.0
MA13.density:2.7
MA13.minrunnumber:159		    
MA13.maxrunnumber:164	
MA13.maxanglenumber:50000

MA14.exist:1            	
MA14.ucenter:6.0
MA14.vcenter:2.0
MA14.ulength:1.5
MA14.vlength:1.5
MA14.thickness:6.0
MA14.atomicnumber:13.0
MA14.atomicmass:27.0
MA14.density:2.7
MA14.minrunnumber:159		    
MA14.maxrunnumber:164	
MA14.maxanglenumber:50000

MA15.exist:1            	
MA15.ucenter:6.0
MA15.vcenter:-3.0
MA15.ulength:1.5
MA15.vlength:1.5
MA15.thickness:6.0
MA15.atomicnumber:13.0
MA15.atomicmass:27.0
MA15.density:2.7
MA15.minrunnumber:159		    
MA15.maxrunnumber:164	
MA15.maxanglenumber:50000

# 0.5 mm Alu
MA16.exist:1
MA16.ucenter:0.0
MA16.vcenter:-0.5
MA16.ulength:1.5
MA16.vlength:1.5
MA16.thickness:0.5
MA16.atomicnumber:13.0
MA16.atomicmass:27.0
MA16.density:2.7
MA16.minrunnumber:138
MA16.maxrunnumber:144
MA16.maxanglenumber:50000

MA17.exist:1            	
MA17.ucenter:-6.0
MA17.vcenter:2.0
MA17.ulength:1.5
MA17.vlength:1.5
MA17.thickness:0.5
MA17.atomicnumber:13.0
MA17.atomicmass:27.0
MA17.density:2.7
MA17.minrunnumber:138		    
MA17.maxrunnumber:144	
MA17.maxanglenumber:50000	    

MA18.exist:1            	
MA18.ucenter:-6.0
MA18.vcenter:-3.0
MA18.ulength:1.5
MA18.vlength:1.5
MA18.thickness:0.5
MA18.atomicnumber:13.0
MA18.atomicmass:27.0
MA18.density:2.7
MA18.minrunnumber:138		    
MA18.maxrunnumber:144	
MA18.maxanglenumber:50000

MA19.exist:1            	
MA19.ucenter:6.0
MA19.vcenter:2.0
MA19.ulength:1.5
MA19.vlength:1.5
MA19.thickness:0.5
MA19.atomicnumber:13.0
MA19.atomicmass:27.0
MA19.density:2.7
MA19.minrunnumber:138		    
MA19.maxrunnumber:144	
MA19.maxanglenumber:50000

MA20.exist:1            	
MA20.ucenter:6.0
MA20.vcenter:-3.0
MA20.ulength:1.5
MA20.vlength:1.5
MA20.thickness:0.5
MA20.atomicnumber:13.0
MA20.atomicmass:27.0
MA20.density:2.7
MA20.minrunnumber:138		    
MA20.maxrunnumber:144	
MA20.maxanglenumber:50000

# 1.5 mm Alu
MA21.exist:1
MA21.ucenter:0.0
MA21.vcenter:-0.5
MA21.ulength:1.5
MA21.vlength:1.5
MA21.thickness:1.5
MA21.atomicnumber:13.0
MA21.atomicmass:27.0
MA21.density:2.7
MA21.minrunnumber:130
MA21.maxrunnumber:137
MA21.maxanglenumber:50000

MA22.exist:1            	
MA22.ucenter:-6.0
MA22.vcenter:2.0
MA22.ulength:1.5
MA22.vlength:1.5
MA22.thickness:1.5
MA22.atomicnumber:13.0
MA22.atomicmass:27.0
MA22.density:2.7
MA22.minrunnumber:130		    
MA22.maxrunnumber:137	
MA22.maxanglenumber:50000	    

MA23.exist:1            	
MA23.ucenter:-6.0
MA23.vcenter:-3.0
MA23.ulength:1.5
MA23.vlength:1.5
MA23.thickness:1.5
MA23.atomicnumber:13.0
MA23.atomicmass:27.0
MA23.density:2.7
MA23.minrunnumber:130		    
MA23.maxrunnumber:137	
MA23.maxanglenumber:50000

MA24.exist:1            	
MA24.ucenter:6.0
MA24.vcenter:2.0
MA24.ulength:1.5
MA24.vlength:1.5
MA24.thickness:1.5
MA24.atomicnumber:13.0
MA24.atomicmass:27.0
MA24.density:2.7
MA24.minrunnumber:130		    
MA24.maxrunnumber:137	
MA24.maxanglenumber:50000

MA25.exist:1            	
MA25.ucenter:6.0
MA25.vcenter:-3.0
MA25.ulength:1.5
MA25.vlength:1.5
MA25.thickness:1.5
MA25.atomicnumber:13.0
MA25.atomicmass:27.0
MA25.density:2.7
MA25.minrunnumber:130		    
MA25.maxrunnumber:137	
MA25.maxanglenumber:50000


# 3 mm Alu
MA26.exist:1
MA26.ucenter:0.0
MA26.vcenter:-0.5
MA26.ulength:1.5
MA26.vlength:1.5
MA26.thickness:3.0
MA26.atomicnumber:13.0
MA26.atomicmass:27.0
MA26.density:2.7
MA26.minrunnumber:145
MA26.maxrunnumber:150
MA26.maxanglenumber:50000

MA27.exist:1            	
MA27.ucenter:-6.0
MA27.vcenter:2.0
MA27.ulength:1.5
MA27.vlength:1.5
MA27.thickness:3.0
MA27.atomicnumber:13.0
MA27.atomicmass:27.0
MA27.density:2.7
MA27.minrunnumber:145		    
MA27.maxrunnumber:150	
MA27.maxanglenumber:50000	    

MA28.exist:1            	
MA28.ucenter:-6.0
MA28.vcenter:-3.0
MA28.ulength:1.5
MA28.vlength:1.5
MA28.thickness:3.0
MA28.atomicnumber:13.0
MA28.atomicmass:27.0
MA28.density:2.7
MA28.minrunnumber:145		    
MA28.maxrunnumber:150	
MA28.maxanglenumber:50000

MA29.exist:1            	
MA29.ucenter:6.0
MA29.vcenter:2.0
MA29.ulength:1.5
MA29.vlength:1.5
MA29.thickness:3.0
MA29.atomicnumber:13.0
MA29.atomicmass:27.0
MA29.density:2.7
MA29.minrunnumber:145		    
MA29.maxrunnumber:150	
MA29.maxanglenumber:50000

MA30.exist:1            	
MA30.ucenter:6.0
MA30.vcenter:-3.0
MA30.ulength:1.5
MA30.vlength:1.5
MA30.thickness:3.0
MA30.atomicnumber:13.0
MA30.atomicmass:27.0
MA30.density:2.7
MA30.minrunnumber:145		    
MA30.maxrunnumber:150	
MA30.maxanglenumber:50000
